/* eslint-disable @next/next/no-img-element */
import Link from "next/link"
import { JSX, SVGProps } from "react"

export function MainPage() {
  return (
    <div className="flex flex-col min-h-[100dvh]">
      <header className="px-4 lg:px-6 py-8 lg:py-14 flex flex-col items-center justify-center text-center">
        <div className="space-y-2">
          <CodeIcon className="h-8 w-8 mx-auto" />
          <h1 className="text-3xl font-bold tracking-tighter sm:text-5xl">Dawid Chmist</h1>
          <p className="text-sm text-gray-500 dark:text-gray-400">DevOps Engineer</p>
        </div>
      </header>
      <main className="flex-1">
        <section className="w-full py-12 md:py-24 lg:py-32 border-t">
          <div className="container grid items-center gap-4 px-4 text-center md:px-6 max-w-none">
            <div className="space-y-3">
              <h2 className="text-3xl font-bold tracking-tighter md:text-4xl/tight">Skills</h2>
              <p className="mx-auto max-w-[600px] text-gray-500 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed dark:text-gray-400">
                I am a DevOps Engineer with a passion for software development. I have experience in Ansible, Docker, Kubernetes, and more.
              </p>
            </div>
            <div className="mx-auto grid max-w-sm items-start gap-1.5 md:grid-cols-2 lg:max-w-4xl lg:gap-3 lg:grid-cols-3">
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Ansible
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Docker
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Terraform
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Linux
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Kubernetes
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Go
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                JS/TS
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Python
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                PostgreSQL
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                MySQL
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                MongoDB
              </div>
              <div className="inline-flex rounded-full bg-gray-100 px-4 py-2 items-center text-sm dark:bg-gray-800">
                <CheckCircleIcon className="w-4 h-4 mr-2.5" />
                Kubernetes
              </div>

            </div>
          </div>
        </section>
        <section className="w-full py-12 md:py-24 lg:py-32 border-t max-w-none">
          <div className="container grid items-center gap-4 px-4 text-center md:px-6 max-w-none">
            <div className="space-y-3">
              <h2 className="text-3xl font-bold tracking-tighter md:text-4xl/tight">Projects</h2>
              <p className="mx-auto max-w-[600px] text-gray-500 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed dark:text-gray-400">
                Check out my latest projects on GitLab.
              </p>
            </div>
            <div className="mx-auto grid max-w-5xl items-center gap-6 py-12 lg:grid-cols-2 lg:gap-12">
              <div className="grid gap-1">
              <Link href="https://gitlab.com/chmist.dawid/portfolio-next"><h3 className="text-lg font-bold underline">Portfolio</h3></Link>
                <div className="text-sm text-gray-500 dark:text-gray-400">
                  The Portfolio that currently you are looking at. It has been created using Next.js and Tailwind CSS.
                  That was practical test of V0 project of Vercel.
                </div>
              </div>
              <div className="grid gap-1">
              <Link href="https://gitlab.com/chmist.dawid/raytracer-in-go"><h3 className="text-lg font-bold underline">Simple Raytracer written in Go</h3></Link>
                <div className="text-sm text-gray-500 dark:text-gray-400">
                  That is a simple raytracer written in Go. It is a project that I have created to learn more Go and 3D graphics.
                </div>
              </div>
              <div className="grid gap-1">
              <Link href="https://gitlab.com/chmist.dawid/qdsh"><h3 className="text-lg font-bold underline">qdsh</h3></Link>
                <div className="text-sm text-gray-500 dark:text-gray-400">
                  Qdsh is a shell written in Go. It is a project that I have created to learn more about potential usage of Go in system programming.
                </div>
              </div>
              <div className="grid gap-1">
              <Link href="https://gitlab.com/chmist.dawid/shrt"><h3 className="text-lg font-bold underline">shrt</h3></Link>
                <div className="text-sm text-gray-500 dark:text-gray-400">
                  Shrt is a URL shortener written in Go. It is a project that I have created to learn more about potential usage of Go in web development.
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="w-full py-12 md:py-24 lg:py-32 border-t">
          <div className="container grid items-center gap-4 px-4 text-center md:px-6 max-w-none">
            <div className="space-y-3">
              <h2 className="text-3xl font-bold tracking-tighter md:text-4xl/tight">Contact Me</h2>
              <p className="mx-auto max-w-[600px] text-gray-500 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed dark:text-gray-400">
                Want to collaborate? Send me a message.
              </p>
              <div>Email: chmistdawid@gmail.com</div>
              <div>
                <Link href="https://gitlab.com/chmist.dawid">
                  <p className="text-blue-500 hover:underline">GitLab</p>
                </Link>
              </div>
              <div>
                <Link href="https://www.linkedin.com/in/dawid-c-9741791a2/">
                  <p className="text-blue-500 hover:underline">LinkedIn</p>
                </Link>
              </div>
            </div>
          </div>

        </section>
      </main>
      <footer className="flex flex-col gap-2 sm:flex-row py-6 w-full shrink-0 items-center px-4 md:px-6 border-t">
        <p className="text-xs text-gray-500 dark:text-gray-400">© 2024 Dawid Chmist. All rights reserved.</p>
        <nav className="sm:ml-auto flex gap-4 sm:gap-6">
          <a className="text-xs hover:underline underline-offset-4" href="mailto:chmistdawid@gmail.com">
            Email
          </a>
          <a className="text-xs hover:underline underline-offset-4" href="https://gitlab.com/chmist.dawid">
            GitLab
          </a>
          <a className="text-xs hover:underline underline-offset-4" href="https://www.linkedin.com/in/dawid-c-9741791a2/">
            LinkedIn
          </a>
        </nav>
      </footer>
    </div>
  )
}

function CheckCircleIcon(props: JSX.IntrinsicAttributes & SVGProps<SVGSVGElement>) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14" />
      <polyline points="22 4 12 14.01 9 11.01" />
    </svg>
  )
}


function CodeIcon(props: JSX.IntrinsicAttributes & SVGProps<SVGSVGElement>) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <polyline points="16 18 22 12 16 6" />
      <polyline points="8 6 2 12 8 18" />
    </svg>
  )
}
